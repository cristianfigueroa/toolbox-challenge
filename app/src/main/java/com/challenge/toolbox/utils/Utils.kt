package com.challenge.toolbox.utils

import android.text.TextUtils
import android.util.Patterns
import com.challenge.toolbox.MyApp

object Utils {

    fun isEmailValid(email: String) : Boolean {
        return !TextUtils.isEmpty(email) &&  Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
    fun savePreferencesString(key: String, value: String) {
        MyApp.sharedPreferences.edit().putString(key, value).apply()
    }
    fun getPreferences(key: String, defaultValue: String = ""): String {
        return MyApp.sharedPreferences.getString(key, defaultValue)!!
    }
}