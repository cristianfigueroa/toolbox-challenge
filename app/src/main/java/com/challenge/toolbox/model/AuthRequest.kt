package com.challenge.toolbox.model

data class AuthRequest (val sub: String)