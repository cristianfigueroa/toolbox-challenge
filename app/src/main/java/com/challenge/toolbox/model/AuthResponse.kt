package com.challenge.toolbox.model

data class AuthResponse(val sub: String, val token: String, val type: String)
