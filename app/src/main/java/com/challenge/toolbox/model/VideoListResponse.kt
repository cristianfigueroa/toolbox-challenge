package com.challenge.toolbox.model

data class VideoListResponse(val videos: List<VideoModel>)
