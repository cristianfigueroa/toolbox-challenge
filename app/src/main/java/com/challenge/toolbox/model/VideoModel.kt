package com.challenge.toolbox.model

import com.google.gson.annotations.SerializedName

enum class Type {
    THUMB, POSTER
}
data class VideoItem (
        val title: String,
        val imageUrl: String,
        val videoUrl: String,
        val description: String,
        )
data class VideoModel (
    val title: String,
    val type: String,
    @SerializedName("items") val videos: List<VideoItem>,
)
