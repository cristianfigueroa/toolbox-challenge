package com.challenge.toolbox.service

import com.challenge.toolbox.model.VideoListResponse
import com.challenge.toolbox.model.VideoModel
import retrofit2.Response
import retrofit2.http.GET

interface VideoService {
    @GET("v1/mobile/data")
    suspend fun getVideos(): Response<List<VideoModel>>

}