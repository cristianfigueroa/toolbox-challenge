package com.challenge.toolbox.service

import com.challenge.toolbox.model.AuthRequest
import com.challenge.toolbox.model.AuthResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthService {
    @POST("v1/mobile/auth")
    suspend fun authentication(@Body request: AuthRequest): Response<AuthResponse>
    @POST("v1/mobile/auth")
    fun refreshToken(@Body request: AuthRequest): Call<AuthResponse>
}