package com.challenge.toolbox.config

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitHelper {
    val baseUrl = "https://echo-serv.tbxnet.com/"

    fun getInstance(): Retrofit {
        val okHttpClient = OkHttpClient().newBuilder().addInterceptor(AuthInterceptor()).build()
        return Retrofit.Builder().baseUrl(baseUrl)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}