package com.challenge.toolbox.config


import com.challenge.toolbox.model.AuthRequest
import com.challenge.toolbox.model.AuthResponse
import com.challenge.toolbox.service.AuthService
import com.challenge.toolbox.utils.Utils
import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

private const val  authTokenKey = "AuthToken"
private const val typeKey = "TypeKey"

class AuthInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
       val original = chain.request()
        if (original.url().toString().contains("auth")){
            val response = chain.proceed(original)
            if (response.isSuccessful) {
                val result = response.peekBody(Long.MAX_VALUE)
                val authResponse = Gson().fromJson(result.string(), AuthResponse::class.java)
                saveToken(authResponse.token, authResponse.type)
            }
            return response
        }
        val token = Utils.getPreferences(authTokenKey)
        val type = Utils.getPreferences(typeKey)
        val response = executeNewRequest(AuthResponse("", token, type), original, chain)
        when(response.code()) {
            401 -> {
                val authService = RetrofitHelper.getInstance().create(AuthService::class.java)
                val responseRefresh = authService.refreshToken(AuthRequest("ToolboxMobileTest")).execute()
                if (responseRefresh.isSuccessful){
                    val authResponse = responseRefresh.body()!!
                    saveToken(authResponse.token, authResponse.type)
                    response.close()
                    return  executeNewRequest(authResponse, original, chain)
                }
            }
        }
        return response
    }
    private fun executeNewRequest(token: AuthResponse, request: Request, chain: Interceptor.Chain): Response {
        val newRequest = request.newBuilder().addHeader("Authorization", "${token.type} ${token.token}")
        return chain.proceed(newRequest.build())
    }
    private fun saveToken (token: String, type: String) {
        Utils.savePreferencesString(authTokenKey, token )
        Utils.savePreferencesString(typeKey, type)
    }


}