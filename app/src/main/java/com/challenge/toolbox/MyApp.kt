package com.challenge.toolbox

import android.app.Application
import android.content.SharedPreferences

class MyApp: Application() {

    override fun onCreate() {
        super.onCreate()
        MyApp.sharedPreferences = applicationContext.getSharedPreferences(BuildConfig.APPLICATION_ID + "global", MODE_PRIVATE)
    }

    companion object {
        lateinit  var sharedPreferences: SharedPreferences

    }
}