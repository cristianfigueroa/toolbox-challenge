package com.challenge.toolbox.ui.fragments.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.challenge.toolbox.databinding.VideoListBinding
import com.challenge.toolbox.model.VideoModel

class HomeAdapter (var videoModelList: List<VideoModel>) : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: VideoListBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = VideoListBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = videoModelList.get(position)
        val binding = holder.binding
        binding.title.text = item.title
        val videoAdapter = VideoAdapter(videoItemList = item.videos, type = item.type)
        binding.videoListRecyclerview.layoutManager = LinearLayoutManager(binding.videoListRecyclerview.context,
            LinearLayoutManager.HORIZONTAL, false)
        binding.videoListRecyclerview.adapter = videoAdapter
    }

    override fun getItemCount(): Int {
        return videoModelList.size
    }

}