package com.challenge.toolbox.ui.fragments.home.adapter

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.signature.ObjectKey
import com.challenge.toolbox.databinding.VideoItemMainBinding
import com.challenge.toolbox.model.VideoItem

class VideoAdapter (var videoItemList: List<VideoItem>, var type: String) : RecyclerView.Adapter<VideoAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: VideoItemMainBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = VideoItemMainBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = videoItemList[position]
        val imageUrl = item.imageUrl
        val title = item.title
        val keyImage = "${item.imageUrl}_${item.title}"
        if (type == "thumb") {
            holder.binding.includePoster.root.visibility = View.GONE
            holder.binding.includeThumb.root.visibility = View.VISIBLE
            setImage(imageUrl, holder.binding.includeThumb.image, keyImage)
            setTitle(title, holder.binding.includeThumb.title)
        } else {
            holder.binding.includePoster.root.visibility = View.VISIBLE
            holder.binding.includeThumb.root.visibility = View.GONE
            setImage(imageUrl, holder.binding.includePoster.image, keyImage)
            setTitle(title, holder.binding.includePoster.title)
        }

    }

    private fun setTitle (title: String, textView: TextView) {
        textView.text = title
    }

    private fun setImage (url: String, imageView: ImageView, key: String) {
        Glide.with(imageView.context).
        load(url).placeholder(ColorDrawable(Color.GRAY)).signature(ObjectKey(key)).centerCrop().into(imageView)
    }

    override fun getItemCount(): Int {
        return videoItemList.size
    }

}