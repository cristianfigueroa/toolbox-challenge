package com.challenge.toolbox.ui.fragments.home

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.challenge.toolbox.R
import com.challenge.toolbox.config.RetrofitHelper
import com.challenge.toolbox.service.VideoService
import com.challenge.toolbox.ui.LoadingView
import com.challenge.toolbox.ui.fragments.home.adapter.HomeAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * A fragment representing a list of Items.
 */
class HomeFragment : Fragment() {

    lateinit var recyclerView: RecyclerView
    lateinit var loadingView: Dialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        recyclerView = view.findViewById<RecyclerView>(R.id.home_list)
        loadingView = LoadingView.showDialog(view.context)
        return view
    }

    override fun onResume() {
        super.onResume()
        CoroutineScope(Dispatchers.IO).launch {
            val videoService = RetrofitHelper.getInstance().create(VideoService::class.java)
            val response = videoService.getVideos()
            if (response.isSuccessful) {
                activity?.runOnUiThread {
                    recyclerView.layoutManager = LinearLayoutManager(context)
                    recyclerView.adapter = HomeAdapter(response.body() ?: listOf())
                    loadingView.hide()
                }
            }
        }
    }
}