package com.challenge.toolbox.ui.fragments.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.navigation.findNavController
import com.challenge.toolbox.R
import com.challenge.toolbox.utils.Utils
import com.challenge.toolbox.config.RetrofitHelper
import com.challenge.toolbox.model.AuthRequest
import com.challenge.toolbox.service.AuthService
import com.challenge.toolbox.ui.LoadingView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.fragment_login, container, false)
        val emailEditText = view.findViewById<EditText>(R.id.editTextTextEmailAddress)
        val button = view.findViewById<Button>(R.id.button)
        button.setOnClickListener {
            if(Utils.isEmailValid(emailEditText.text.toString().trim())) {
                val loadingView = LoadingView.showDialog(view.context)
                CoroutineScope(Dispatchers.IO).launch {
                    val authService = RetrofitHelper.getInstance().create(AuthService::class.java)
                    val request = AuthRequest("ToolboxMobileTest")
                    val response = authService.authentication(request)
                    if (response.isSuccessful) {
                        activity?.runOnUiThread {
                            LoadingView.hideDialog(loadingView)
                            view.findNavController().navigate(R.id.homeFragment)
                        }
                    }
                }

            } else {
                Toast.makeText(context, "Ingrese un correo electronico valido", Toast.LENGTH_SHORT).show()
            }
        }
        return view
    }

}