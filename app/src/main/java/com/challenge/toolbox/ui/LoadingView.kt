package com.challenge.toolbox.ui

import android.app.Dialog
import android.content.Context
import android.view.Window
import com.challenge.toolbox.R

object LoadingView {

     fun showDialog(context: Context): Dialog {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.loading)
        dialog.show()
        return dialog
    }
    fun hideDialog(dialog: Dialog) {
        dialog.hide()
    }

}